<?php

$EmailFrom = "admin@wisconsinbikefed.org";
$EmailTo = "chris.aalid@wisconsinbikefed.org";
$Subject = "Ciclovia MKE Contact Form";
$ContactName = Trim(stripslashes($_POST['ContactName'])); 
$BizName = Trim(stripslashes($_POST['BizName'])); 
$Email = Trim(stripslashes($_POST['Email'])); 
$Phone = Trim(stripslashes($_POST['Phone'])); 
$Address = Trim(stripslashes($_POST['Address'])); 
$City = Trim(stripslashes($_POST['City'])); 
$State = Trim(stripslashes($_POST['State'])); 
$Zip = Trim(stripslashes($_POST['Zip'])); 
$VendorType = Trim(stripslashes($_POST['VendorType'])); 

// validation
$validationOK=true;
if (!$validationOK) {
  print "<meta http-equiv=\"refresh\" content=\"0;URL=error.htm\">";
  exit;
}

$Body = "";
$Body .= "Contact Name: ";
$Body .= $ContactName;
$Body .= "\n";

$Body = "";
$Body .= "Business Name: ";
$Body .= $ContactName;
$Body .= "\n";

$Body = "";
$Body .= "Email: ";
$Body .= $Email;
$Body .= "\n";

$Body = "";
$Body .= "Phone: ";
$Body .= $Phone;
$Body .= "\n";

$Body = "";
$Body .= "Address: ";
$Body .= $Address;
$Body .= "\n";

$Body = "";
$Body .= "City: ";
$Body .= $City;
$Body .= "\n";

$Body = "";
$Body .= "State: ";
$Body .= $State;
$Body .= "\n";

$Body = "";
$Body .= "Zip: ";
$Body .= $Zip;
$Body .= "\n";

$Body = "";
$Body .= "Vendor Type: ";
$Body .= $VendorType;
$Body .= "\n";

// send email 
$success = mail($EmailTo, $Subject, $Body, "From: <$EmailFrom>");

// redirect to success page 
if ($success){
  print "<meta http-equiv=\"refresh\" content=\"0;URL=thanks.php\">";
}
else{
  print "<meta http-equiv=\"refresh\" content=\"0;URL=error.htm\">";
}
?>
<!doctype html>
<html class="no-js" lang="en">
  <head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title><?php echo $site->title()->html() ?> | <?php echo $page->title()->html() ?></title>
    <link href='http://fonts.googleapis.com/css?family=Source+Sans+Pro:400,300,400italic,700' rel='stylesheet' type='text/css'>
    <link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css">
    <?php echo css('assets/css/foundation.min.css'); ?>
    <?php echo css('assets/css/app.css'); ?>
    <?php echo js('assets/js/modernizr.js') ?>
  </head>
  <body>
    <div id="fb-root"></div>
    <script>(function(d, s, id) {
      var js, fjs = d.getElementsByTagName(s)[0];
      if (d.getElementById(id)) return;
      js = d.createElement(s); js.id = id;
      js.src = "//connect.facebook.net/en_US/sdk.js#xfbml=1&version=v2.4&appId=182254448513725";
      fjs.parentNode.insertBefore(js, fjs);
    }(document, 'script', 'facebook-jssdk'));</script>

    <div class="lang__topper">
      <div class="lang__topper__inner">
        <span>Switch Language: </span>
        <a href="http://en.cicloviamke.org">English</a>
        <a href="http://cicloviamke.org">Spanish</a>
      </div>
    </div>

    <div class="contain-to-grid">
      <nav class="top-bar" data-topbar role="navigation">
        <ul class="title-area">
          <li class="name">
            <h1><a href="<?php echo url() ?>"><img src="<?php echo $site->url() ?>/assets/img/nav-logo.png" alt="Logo"></a></h1>
          </li>
          <li class="toggle-topbar menu-icon"><a href="#"><span></span></a></li>
        </ul>
      
        <section class="top-bar-section" id="right-nav">
          <!-- Right Nav Section -->
          <ul class="right">
            <?php foreach($pages->visible() as $p): ?>
              <li>
                <a <?php e($p->isOpen(), ' class="active"') ?> href="<?php echo $p->url() ?>"><?php echo $p->title()->html() ?></a>
              </li>
            <?php endforeach ?>
          </ul>
        </section>

      </nav>
    </div><!-- // NAVBAR -->
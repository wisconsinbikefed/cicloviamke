      <div class="full__container sponsors__footer">
        <div class="row">
          <div class="large-12 large-centered columns">
            <h3>Presented By</h3>
          </div>
        </div>
        <div class="row sponsors__logos">
          <?php foreach($site->files()->limit(6) as $file): ?>
            <div class="small-6 medium-4 large-2 columns">
              <img src="<?php echo $file->url() ?>" alt="" class="sponsors__logo">
            </div>
          <?php endforeach ?>
        </div>
      </div>

      <div class="full__container footer">
        <div class="row">
          <div class="large-4 columns">
            <ul class="footer__social">
              <li><a href="http://facebook.com/cicloviamke"><i class="fa fa-facebook-official fa-2x"></i></a></li>
              <li><a href="http://twitter.com/"><i class="fa fa-twitter-square fa-2x"></i></a></li>
            </ul>
          </div>
          <div class="large-8 columns">
            <span class="footer__contact">
              <a href="<?php echo url() ?>/contact">Contact</a>
              <a href="<?php echo url() ?>/sponsors">Sponsors</a>
            </span>
          </div>
        </div>
      </div>
      
      
      <?php echo js('assets/js/vendor/jquery.js') ?>
      <?php echo js('assets/js/foundation.min.js') ?>
      <script>
        $(document).foundation();
      </script>
    </body>
  </html>
<?php if(!defined('KIRBY')) exit ?>

username: admin
email: admin+cicloviamke@wisconsinbikefed.org
password: >
  $2a$10$vtzsk0fRAqtgnmRvdBmFFOOAzp/UKc1qJOq9KMf9CF/yMLG5ZeO.O
language: en
role: admin
history:
  - route
  - get-involved
  - get-a-spot
  - contact

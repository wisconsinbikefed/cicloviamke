<?php snippet('header') ?>
	<div class="row page__title">
	  <div class="large-12 columns">
	    <h2><?php echo $page->title()->html() ?></h2>
	  </div>
	</div>

	<div class="row main__body__row">
      <div class="large-8 columns main__body__container">
		<?php echo $page->text()->kirbytext() ?>

		<form action="<?php echo url('assets/contact-get-a-spot.php') ?>" accept-charset="UTF-8" method="POST">
			<div class="row">
			  <div class="large-12 columns">
			  	<label>Name</label>
			    <input type="text" placeholder="Name" name="ContactName" />
			  </div>
			</div>
			<div class="row">
			  <div class="large-12 columns">
			  	<label>Business Name</label>
			    <input type="text" placeholder="Business Name" name="BizName" />
			  </div>
			</div>
			<div class="row">
			  <div class="large-6 columns">
			  	<label>Email</label>
			    <input type="email" placeholder="Email" name="Email" />
			  </div>
			  <div class="large-6 columns">
			  	<label>Phone</label>
			    <input type="text" placeholder="Phone" name="Phone" />
			  </div>
			</div>
			<div class="row">
			  <div class="large-12 columns">
			  	<label>Addres</label>
			    <input type="text" placeholder="Address" name="Address">
			  </div>
			</div>
			<div class="row">
			  <div class="large-4 columns">
			  	<label>City</label>
			    <input type="text" placeholder="City" name="City" / >
			  </div>
			  <div class="large-4 columns">
			  	<label>State</label>
			    <input type="text" placeholder="State" name="State">
			  </div>
			  <div class="large-4 columns">
			  	<label>Zip</label>
			    <input type="text" placeholder="Zip" name="Zip">
			  </div>
			</div>
			<div class="row">
			  <div class="large-12 columns">
			    <label>I represent...</label>
			    <select name="VendorType" id="VendorType">
			      <option value="EventSponsor">An event sponsor</option>
			      <option value="CommunityOrganization">A community organization</option>
			      <option value="FoodVendor">A food vendor</option>
			      <option value="LocalBusiness">A local business</option>
			      <option value="OutsideBusiness">A business outside the immediate event area</option>
			      <option value="MilwaukeeNonProfit">A Milwaukee nonprofit organization</option>
			    </select>
			  </div>
			</div>
		  <div class="row">
		    <div class="large-12 columns">
		      <button value="Submit" type="submit" class="button expand">Enviar</button>
		    </div>
		  </div>
		</form>

      </div>
      <?php snippet('sidebar') ?>
    </div>

<?php snippet('footer') ?>
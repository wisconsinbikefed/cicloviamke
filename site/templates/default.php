<?php snippet('header') ?>
	<div class="row page__title">
	  <div class="large-12 columns">
	    <h2><?php echo $page->title()->html() ?></h2>
	  </div>
	</div>

	<div class="row main__body__row">
      <div class="large-8 columns main__body__container">
		<?php echo $page->text()->kirbytext() ?>
      </div>
      <?php snippet('sidebar') ?>
    </div>

<?php snippet('footer') ?>
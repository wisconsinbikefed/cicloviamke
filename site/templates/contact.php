<?php snippet('header') ?>
    <div class="row page__title">
      <div class="large-12 columns">
        <h2><?php echo $page->title()->html() ?></h2>
      </div>
    </div>

    <div class="row main__body__row">
      <div class="large-8 columns main__body__container">
        <?php echo $page->text()->kirbytext() ?>

        <form action="<?php echo url('assets/contactengine.php') ?>" accept-charset="UTF-8" method="POST">
          <div class="row">
            <div class="large-12 columns">
              <label>Name</label>
              <input type="text" placeholder="Name" name="Name" />
            </div>
          </div>
          <div class="row">
            <div class="large-6 columns">
              <label>Email</label>
              <input type="email" name="Email" placeholder="Email" />
            </div>
            <div class="large-6 columns">
              <label>Phone</label>
              <input type="text" name="Phone" placeholder="Phone" />
            </div>
          </div>
          <div class="row">
            <div class="large-12 columns">
              <label>Choose One</label>
              <select name="ContactReason" id="ContactReason">
                <option value="VolunteerInfo">I would like to volunteer</option>
                <option value="Sponsorship">I'm interested in sponsorship</option>
                <option value="Press">I would like to submit a press inquiry</option>
              </select>
            </div>
          </div>
          <div class="row">
            <div class="large-12 columns">
              <button value="Submit" type="submit" class="button expand">Submit</button>
            </div>
          </div>
        </form>

      </div>
      <?php snippet('sidebar') ?>
    </div>

<?php snippet('footer') ?>
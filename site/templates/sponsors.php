<?php snippet('header') ?>
	<div class="row page__title">
	  <div class="large-12 columns">
	    <h2><?php echo $page->title()->html() ?></h2>
	  </div>
	</div>

	<div class="row main__body__row">
      <div class="large-8 columns main__body__container">
		<?php echo $page->text()->kirbytext() ?>
      </div>
      <div class="large-8 columns main__body__container">

		<ul class="small-block-grid-3">
			<?php foreach($page->files() as $file): ?>
			    <li>
			    	<img src="<?php echo $file->url() ?>" class="sponsors__page__logo">
			    </li>
			<?php endforeach ?>
		</ul>
      </div>
      <?php snippet('sidebar') ?>
    </div>

<?php snippet('footer') ?>
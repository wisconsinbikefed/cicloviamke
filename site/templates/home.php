<?php snippet('header') ?>

	<div class="hero__container"></div>

	<div class="row main__body__row">
	  <div class="large-8 large-offset-2 columns main__body__container">
	    <p class="subhead">
	      La ruta empieza en la Avenida Greenfield norte a la Calle Washington sobre la Avenida Cesar Chavez, despues dobla este en la Calle Washington hacia la Calle Barclay, y termina en la apertura del KK River Trail.
	    </p>
		<p class="subhead">
			La Ciclovía es una iniciativa que cierra las calles temporalmente al tráfico de vehículos automóviles - con el fin de que la gente pueda usarlas para caminar, andar en bicicleta, bailar, jugar, y socializar! Con más de 100 iniciativas documentadas en Norteamérica, ciclovías son más y más comunes en ciudades en búsqueda de formas innovadoras para lograr objetivos ambientales, sociales, económicos y de salud pública.
		</p>
		<p class="subhead">
	      El gol de la Ciclovía inaugural es tener un evento en el Southside de Milwaukee que promoverá la actividad física, la salud ambiental, el acceso a recursos comunitarios, el orgullo en los vecindarios, y conexiones entre diferentes partes geográficas de la ciudad. ¡Adelante!  
	    </p>
	  </div>
	</div>

	<div class="full__container homepage__route">
	  <div class="row">
	    <div class="large-12">
	      <h3>Get out and explore your community</h3>
	      <a href="/route" class="button radius">See the route</a>
	    </div>
	  </div>
	</div>

<?php snippet('footer') ?>
<?php if(!defined('KIRBY')) exit ?>

title: Get a Spot
pages: true
files: true
fields:
  title:
    label: Title
    type:  text
  text:
    label: Text
    type:  textarea